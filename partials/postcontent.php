<div class="story wrapper cf">

    <div class="post_title_section">
        <div class="post_info">
            <div class="post_category font_two">
                <?php the_category(' , '); ?>
            </div>
            <div class="post_comments_count">
            <i class="fa fa-commenting-o" aria-hidden="true"></i>
            <?php comments_number(); ?>
            </div>
            <div class="post_date">
            <i class="fa fa-clock-o" aria-hidden="true"></i> 
            <?php the_date(); ?>
            </div>
        </div>
        <h3 class="post_title"><?php the_title(); ?></h3>
        <?php if (ale_get_meta('post_sub_title'));  ?>
        <span class="post_sub_title">
            <?php echo esc_attr (ale_get_meta('post_sub_title')); { ?>
        </span>
        <?php } ?>
    </div>

    <div class="post_content_section">
        <?php the_content();?>

        <?php if(get_the_tags()){ ?>
        <p class="tagsphar"><?php the_tags( esc_html__('Tagged to: ','olins'), ' ', '<br />' );  ?></p>
        <?php } ?>

        <?php wp_link_pages(array(
        'before' => '<p class="post_pages">' . esc_html__('Pages:', 'olins'),
        'after'	 => '</p>',
    )) ?>
    </div>
</div>