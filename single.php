<?php get_header(); ?>
<section class="site_container">
<div class="page_heading">
    <h2 class="page_title"><?php esc_html_e('Our blog', 'gardener'); ?></h2>
</div>
    <!-- Content -->
    <div class="story ale_blog_archive single_post content cf">
        <div class="cf">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <?php get_template_part('partials/posthead' );?>
            <?php get_template_part('partials/postcontent' );?>
            <?php get_template_part('partials/postfooter' );?>
            <?php endwhile; else: ?>
            <?php get_template_part('partials/notfound')?>
            <?php endif; ?>
        </div>
        <?php if (comments_open() || get_comments_number()) : ?>
        <?php comments_template(); ?>
        <?php endif; ?>
    </div>
</section>
<!-- /.site_container -->
<?php get_footer(); ?>