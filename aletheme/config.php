<?php
/**
 * Get current theme options
 * 
 * @return array
 */
function ale_get_options() {

    $headerfont = array_merge(ale_get_safe_webfonts(), ale_get_google_webfonts());

    $background_defaults = array(
        'color' => '',
        'image' => '',
        'repeat' => 'repeat',
        'position' => 'top center',
        'attachment'=>'scroll',
        'background-size'=>'cover'
    );

    $wrapper_size = array(
        '100' => esc_html__('Flexible 100%','olins'),
        '960' => esc_html__('Boxed 960px','olins'),
        '1024' => esc_html__('Boxed 1024px','olins'),
        'custom' => esc_html__('Custom','olins')
    );

    $design_variant = array(
        //'ikea' => esc_html__('Olins Ikea','olins'),
        'blackwhite' => esc_html__('Olins Black & White','olins'),
        'zoo' => esc_html__('Olins Zoo','olins'),
        'bakery' => esc_html__('Olins Bakery','olins'),
        'photography' => esc_html__('Olins Photography','olins'),
        'luxuryshoes' => esc_html__('Olins Luxury Shoes','olins'),
        'camping' => esc_html__('Olins Camping','olins'),
        'travelphoto' => esc_html__('Olins Travel Photography','olins'),
        'viaje' => esc_html__('Olins Viaje','olins'),
        'wedding' => esc_html__('Olins Wedding','olins'),
        'furniture' => esc_html__('Olins Furniture','olins'),
        'magazine' => esc_html__('Olins Magazine','olins'),
        'creative' => esc_html__('Olins Creative','olins'),
        'brigitte' => esc_html__('Olins Photography Brigitte','olins'),
        'corporate' => esc_html__('Olins Corporate','olins'),
        'cv' => esc_html__('Olins CV','olins'),
        'fashion' => esc_html__('Olins Fashion Store','olins'),
        'pastel' => esc_html__('Olins Pastel Photography','olins'),
        'stephanie' => esc_html__('Olins Stephanie Lark (Wedding)','olins'),
        'cameron' => esc_html__('Olins Cameron','olins'),
        'pixel' => esc_html__('Olins Pixel','olins'),
        'jade' => esc_html__('Olins Jade','olins'),
    );
    $yes_no = array(
        'no' => esc_html__('No. Use Default Styles','olins'),
        'yes' => esc_html__('Yes, Overwrite with Custom','olins')
    );
    $show_hide = array(
        'hide' => esc_html__('Hide','olins'),
        'show' => esc_html__('Show','olins')
    );
    $date_position = array(
        'onimage' => esc_html__('On Featured Image','olins'),
        'infoline' => esc_html__('Blog Info Line','olins'),
        'beforetitle' => esc_html__('Before Title','olins')
    );
    $postline_position = array(
        'beforetitle' => esc_html__('Before Post Title','olins'),
        'aftertitle' => esc_html__('After Post Title','olins'),
        'aftercontent' => esc_html__('After Post Description','olins'),
        'disable' => esc_html__('Disable Post Line','olins')
    );
    $archive_columns = array(
        '1' => esc_html__('One Column','olins'),
        '2' => esc_html__('Two Columns','olins'),
        '3' => esc_html__('Three Columns','olins'),
        '4' => esc_html__('Four Columns','olins'),
        '5' => esc_html__('Five Columns','olins')
    );
    $archive_text_align = array(
        'left' => esc_html__('Left Align','olins'),
        'center' => esc_html__('Center Align','olins'),
        'right' => esc_html__('Right Align','olins'),
        'justify' => esc_html__('Justify','olins')

    );
    $archive_pagination = array(
        'pagination' => esc_html__('Pagination','olins'),
        'loadmore' => esc_html__('Load More Button','olins'),
        'infinite' => esc_html__('Infinite Scroll','olins'),
        'simple' => esc_html__('Simple pagination','olins')
    );
    $archive_sidebar = array(
        'no' => esc_html__('No Sideabar','olins'),
        'left_third' => esc_html__('1/3 Left','olins'),
        'left_fourth' => esc_html__('1/4 Left','olins'),
        'right_third' => esc_html__('1/3 Right','olins'),
        'right_fourth' => esc_html__('1/4 Right','olins')
    );
    $footer_variant = array(
        'default' => esc_html__('Default Footer','olins'),
        'widget' => esc_html__('Widget Footer','olins')
    );
    $page_heading = array(
        'parallax_one' => esc_html__('Dark Title on Light Parallax Image 1','olins'),
        'parallax_two' => esc_html__('Light Title on Dark Parallax Image 2','olins'),
        'simple_image' => esc_html__('White Title on Dark Image','olins'),
        'center_text' => esc_html__('Simple Centered Dart Title (without pre title)','olins'),
        'left_text' => esc_html__('Simple Title (without pre title)','olins'),
        'left_text_breadcrumbs' => esc_html__('Simple Title (without pre title) with Breadcrumbs','olins'),
        'breadcrumbs' => esc_html__('Hide Title, Show Breadcrumbs Line','olins'),
        'wedding_heading' => esc_html__('Left Image. Works for Wedding Variant.','olins'),
        'parallax_three' => esc_html__('Parallax image with Pre Title for Sticky Header','olins')
    );
    $blog_grid = array(
        'masonry' => esc_html__('Masonry Grid','olins'),
        'vintage' => esc_html__('Vintage Grid','olins'),
        'furniture' => esc_html__('Furniture Grid','olins'),
        'magazine' => esc_html__('Magazine Grid','olins'),
        'brigitte' => esc_html__('Brigitte Grid','olins'),
        'cameron' => esc_html__('Cameron Grid','olins'),
        'pixel' => esc_html__('Pixel Grid','olins'),
        'jade' => esc_html__('Jade Grid','olins'),
    );
    $woo_grid = array(
        'default' => esc_html__('Default Style','olins'),
        'vintage' => esc_html__('Vintage Style','olins'),
        'minimal' => esc_html__('Minimal Style','olins'),
        'fashion' => esc_html__('Fashion Style','olins'),
    );
    $menu_styles = array(
        'color' => '',
        'image' => '',
        'width' => '',
        'height' => '',
        'text-color' => '',
        'text-align' => '',
    );

	
	$imagepath =  ALETHEME_URL . '/assets/images/';
	
	$options = array();

    $options[] = array("name" => esc_html__("Brand","olins"),
                        "type" => "heading",
                        "icon" => "fa-desktop");

    $options[] = array( "name" => esc_html__("Site Logo","olins"),
                        "desc" => esc_html__("Upload or put the site logo link.","olins"),
                        "id" => "ale_sitelogo",
                        "std" => "",
                        "type" => "upload");

    $options[] = array( "name" => esc_html__("Footer Logo","olins"),
                        "desc" => esc_html__("Upload or put the footer logo link.","olins"),
                        "id" => "ale_footerlogo",
                        "std" => "",
                        "type" => "upload");

    $options[] = array( "name" => esc_html__("Uplaod a favicon icon","olins"),
                        "desc" => esc_html__("Upload or put the link of your favicon icon","olins"),
                        "id" => "ale_favicon",
                        "std" => "",
                        "type" => "upload");
		
	$options[] = array("name" => esc_html__("Style","olins"),
						"type" => "heading",
                        "icon" => "fa-window-restore");

    $options[] = array( 'name' => esc_html__("Manage Background","olins"),
                        'desc' => esc_html__("Select the background color, or upload a custom background image. Default background is the #f5f5f5 color","olins"),
                        'id' => 'ale_background',
                        'std' => $background_defaults,
                        'type' => 'background');

    $options[] = array("name" => esc_html__("Header Options","olins"),
                        "type" => "heading",
                        "icon" => "fa-phone");

    $options[] = array( "name" => esc_html__("Header phone label","olins"),
                        "desc" => esc_html__("Type your label","olins"),
                        "id" => "ale_header_phone_label",
                        "std" => "Call us",
                        "type" => "text");

    $options[] = array( "name" => esc_html__("Header phone Number","olins"),
                        "desc" => esc_html__("Insert the phone number","olins"),
                        "id" => "ale_header_phone",
                        "std" => "",
                        "type" => "text");

    $options[] = array("name" => esc_html__("Footer Options","olins"),
                        "type" => "heading",
                        "icon" => "fa-copyright");

    $options[] = array( "name" => esc_html__("Footer Call Number","olins"),
                        "desc" => esc_html__("Insert the call number","olins"),
                        "id" => "ale_footer_callnumber",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => esc_html__("Footer Email Address","olins"),
                        "desc" => esc_html__("Insert the Email Address.","olins"),
                        "id" => "ale_footer_email_address",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => esc_html__("Your Address","olins"),
                        "desc" => esc_html__("Insert the Address.","olins"),
                        "id" => "ale_footer_address",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => esc_html__("Copyrights","olins"),
                        "desc" => esc_html__("Insert the Copyrights text","olins"),
                        "id" => "ale_copyrights",
                        "std" => "",
                        "type" => "editor");

    $options[] = array("name" => esc_html__("Page Heading","olins"),
                        "type" => "heading",
                        "icon" => "fa-header");

    $options[] = array( "name" => esc_html__("Page Heading Style","olins"),
                        "desc" => esc_html__("Select a style for page heading. NOTE: Specific Heading was designed and hard coded for a specific demo variant. So after you installed a demo example, do not change this field. It will broke the layout.","olins"),
                        "id" => "ale_page_heading_style",
                        "std" => "parallax_one",
                        "type" => "select",
                        "options" => $page_heading);

    $options[] = array( "name" => esc_html__("Archive Pages Title Background","olins"),
                        "desc" => esc_html__("Upload or put the image link. Necessary size: 1800px-590px","olins"),
                        "id" => "ale_archivetitlebg",
                        "std" => "",
                        "type" => "upload");

    $options[] = array( "name" => esc_html__("Blog Archive Page Title","olins"),
                        "desc" => esc_html__("Specify the title for Blog archive page","olins"),
                        "id" => "ale_archiveblogtitle",
                        "std" => "our blog posts",
                        "type" => "text");

    $options[] = array( "name" => esc_html__("Blog Archive Page Pre Title","olins"),
                        "desc" => esc_html__("Specify the pre title for Blog archive page","olins"),
                        "id" => "ale_archiveblogpretitle",
                        "std" => "take a look at",
                        "type" => "text");

    $options[] = array( "name" => esc_html__("Typography","olins"),
                        "type" => "heading",
                        "icon" => "fa-font");

    $options[] = array( "name" => esc_html__("Select the First Font from Google Library","olins"),
                        "desc" => esc_html__("The default Font is - Arial","olins"),
                        "id" => "ale_font_one",
                        "std" => "Arial",
                        "type" => "select",
                        "options" => $headerfont);

    $options[] = array( "name" => esc_html__("Select the First Font Properties from Google Library","olins"),
                        "desc" => esc_html__("The default Font (extended) is - 400,400i,600,800,800i,900","olins"),
                        "id" => "ale_font_one_ex",
                        "std" => "",
                        "type" => "text",
                        );

    $options[] = array( "name" => esc_html__("Select the Second Font from Google Library","olins"),
                        "desc" => esc_html__("The default Font is - Dosis","olins"),
                        "id" => "ale_font_two",
                        "std" => "Dosis",
                        "type" => "select",
                        "options" => $headerfont);

    $options[] = array( "name" => esc_html__("Select the Second Font Properties from Google Library","olins"),
                        "desc" => esc_html__("The default Font (extended) is - 400i","olins"),
                        "id" => "ale_font_two_ex",
                        "std" => "500,600,700",
                        "type" => "text",
                        );

    $options[] = array( 'name' => esc_html__("H1 Style","olins"),
                        'desc' => esc_html__("Change the h1 style","olins"),
                        'id' => 'ale_h1sty',
                        'std' => array('size' => '32px','face' => 'Dosis','style' => 'normal','transform'=>'none', 'weight'=>'700','lineheight'=>'normal','color' => '#000000'),
                        'type' => 'typography');

    $options[] = array( 'name' => esc_html__("H2 Style","olins"),
                        'desc' => esc_html__("Change the h2 style","olins"),
                        'id' => 'ale_h2sty',
                        'std' => array('size' => '28px','face' => 'Dosis','style' => 'normal','transform'=>'none', 'weight'=>'700','lineheight'=>'normal','color' => '#000000'),
                        'type' => 'typography');

    $options[] = array( 'name' => esc_html__("H3 Style","olins"),
                        'desc' => esc_html__("Change the h3 style","olins"),
                        'id' => 'ale_h3sty',
                        'std' => array('size' => '24px','face' => 'Dosis','style' => 'normal','transform'=>'none', 'weight'=>'700','lineheight'=>'normal','color' => '#000000'),
                        'type' => 'typography');

    $options[] = array( 'name' => esc_html__("H4 Style","olins"),
                        'desc' => esc_html__("Change the h4 style","olins"),
                        'id' => 'ale_h4sty',
                        'std' => array('size' => '20px','face' => 'Dosis','style' => 'normal','transform'=>'none', 'weight'=>'700','lineheight'=>'normal','color' => '#000000'),
                        'type' => 'typography');

    $options[] = array( 'name' => esc_html__("H5 Style","olins"),
                        'desc' => esc_html__("Change the h5 style","olins"),
                        'id' => 'ale_h5sty',
                        'std' => array('size' => '16px','face' => 'Dosis','style' => 'normal','transform'=>'none', 'weight'=>'700','lineheight'=>'normal','color' => '#000000'),
                        'type' => 'typography');

    $options[] = array( 'name' => esc_html__("H6 Style","olins"),
                        'desc' => esc_html__("Change the h6 style","olins"),
                        'id' => 'ale_h6sty',
                        'std' => array('size' => '14px','face' => 'Dosis','style' => 'normal','transform'=>'none', 'weight'=>'700','lineheight'=>'normal','color' => '#898989'),
                        'type' => 'typography');

    $options[] = array( 'name' => esc_html__("Body Style","olins"),
                        'desc' => esc_html__("Change the body font style","olins"),
                        'id' => 'ale_bodystyle',
                        'std' => array('size' => '14px','face' => 'Arial','style' => 'normal','transform'=>'none', 'weight'=>'400','lineheight'=>'24px','color' => '#434343'),
                        'type' => 'typography');

	$options[] = array( "name" => esc_html__("Social Profiles & Share","olins"),
						"type" => "heading",
                        "icon" => "fa-address-book");

    $options[] = array( "name" => esc_html__("Twitter","olins"),
                        "desc" => esc_html__("Your twitter profile URL.","olins"),
                        "id" => "ale_twi",
                        "std" => "",
                        "type" => "text");

	$options[] = array( "name" => esc_html__("Facebook","olins"),
						"desc" => esc_html__("Your facebook profile URL.","olins"),
						"id" => "ale_fb",
						"std" => "",
						"type" => "text");

    $options[] = array( "name" => esc_html__("Youtube","olins"),
                        "desc" => esc_html__("Your youtube profile URL.","olins"),
                        "id" => "ale_you",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => esc_html__("LinkedIn","olins"),
                        "desc" => esc_html__("Your LinkedIn profile URL.","olins"),
                        "id" => "ale_lin",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => esc_html__("Pinterest","olins"),
                        "desc" => esc_html__("Your Pinterest profile URL.","olins"),
                        "id" => "ale_pin",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => esc_html__("Google Plus+","olins"),
                        "desc" => esc_html__("Your Google Plus+ profile URL.","olins"),
                        "id" => "ale_gpl",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => esc_html__("Tumblr","olins"),
                        "desc" => esc_html__("Your Tumblr profile URL.","olins"),
                        "id" => "ale_tum",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => esc_html__("Instagram","olins"),
                        "desc" => esc_html__("Your Instagram profile URL.","olins"),
                        "id" => "ale_insta",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => esc_html__("Reddit","olins"),
                        "desc" => esc_html__("Your Reddit profile URL.","olins"),
                        "id" => "ale_red",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => esc_html__("VK","olins"),
                        "desc" => esc_html__("Your VK profile URL.","olins"),
                        "id" => "ale_vk",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => esc_html__("Share Platforms","olins"),
                        "desc" => esc_html__("Check the platforms you want to use for social share","olins"),
                        "id" => "ale_share_platforms",
                        "std"=>array(
                            'fb'=>'1',
                            'twi'=>'1',
                            'goglp'=>'1'
                            ),
                        "type" => "multicheck",
                        "options" => array(
                            'fb'=>'Facebook',
                            'twi'=>'Twitter',
                            'goglp'=>'Google +',
                            'lin'=>'Linkedin',
                            'red'=>'Reddit',
                            'pin'=>'Pinterest',
                            'vk'=>'Vkontakte'
                        ));

	
	$options[] = array( "name" => esc_html__("Facebook Application ID","olins"),
						"desc" => esc_html__("If you have Application ID you can connect the blog to your Facebook Profile and monitor statistics there.","olins"),
						"id" => "ale_fb_id",
						"std" => "",
						"type" => "text");
	
	$options[] = array( "name" => esc_html__("Enable Open Graph","olins"),
						"desc" => esc_html__("The Open Graph protocol enables any web page to become a rich object in a social graph.","olins"),
						"id" => "ale_og_enabled",
						"std" => "",
						"type" => "checkbox");


	
	$options[] = array( "name" => esc_html__("Advanced Settings","olins"),
						"type" => "heading",
                        "icon" => "fa-cogs");
	
	$options[] = array( "name" => esc_html__("Footer Code","olins"),
						"desc" => esc_html__("If you have anything else to add in the footer - please add it here.","olins"),
						"id" => "ale_footer_info",
						"std" => "",
						"type" => "textarea");

    $options[] = array( "name" => esc_html__("Custom CSS Styles","olins"),
                        "desc" => esc_html__("You can add here your styles. ex. .boxclass { padding:10px; }","olins"),
                        "id" => "ale_customcsscode",
                        "std" => "",
                        "type" => "textarea");

    $options[] = array("name" => esc_html__("Blog Settings","olins"),
                       "type" => "heading",
                       "icon" => "fa-newspaper-o");

    
    $options[] = array( "name" => esc_html__("Custom CSS Styles","olins"),
                       "desc" => esc_html__("You can add here your styles. ex. .boxclass { padding:10px; }","olins"),
                       "id" => "ale_blog_textarea",
                       "std" => "",
                       "type" => "textarea");

    

    // Google maps settings

    $options[] = array("name" => esc_html__("Google Maps","olins"),
                       "type" => "heading",
                       "icon" => "fa-map-marker");

    $options[] = array( "name" => esc_html__("Google Maps API Key","olins"),
                        "desc" => ale_wp_kses(__("Paste a Google Maps API Key. You can generate a key on the  <a href=\"https://console.developers.google.com/apis/\" target=\"_blank\">Google console.</a>","olins")),
                        "id" => "ale_maps_api_key",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => esc_html__("Custom Pin Icon","olins"),
                        "desc" => esc_html__("Upload a Custom Pin Icon. Let it empty to use the default icon.","olins"),
                        "id" => "ale_map_icon",
                        "std" => get_template_directory_uri(). "/css/images/map-icon.png",
                        "type" => "upload");

    $options[] = array( "name" => esc_html__("Map Custom Style","olins"),
                        "desc" => esc_html__("Paste here a custom style for your google map. You can use the snazzymaps.com to take a ready style. ","olins"),
                        "id" => "ale_maps_style",
                        "std" => "",
                        "type" => "text");

	return $options;
}


/**
 * Add Metaboxes
 * @param array $meta_boxes
 * @return array 
 */
function ale_metaboxes($meta_boxes) {
	
	$meta_boxes = array();

    $prefix = "ale_";

    $meta_boxes[] = array(
        'id'         => 'home_settings_metabox',
        'title'      => esc_html__('Home page settings','olins'),
        'pages'      => array( 'page',), // Post type
        'context'    => 'normal',
        'priority'   => 'high',
        'show_on' => array('key' => 'page-template', 'value' => array('template-homepage.php'),),
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
                'name' => esc_html__('Enable order form box','olins'),
                'desc' => esc_html__('Enable or disable order form on homepage','olins'),
                'id'   => $prefix . 'order_form',
                'std'  => 'enable',
                'type'    => 'select',
                'options' => array(
                    array( 'name' => esc_html__('Enable form','olins'), 'value' => 'enable', ),
                    array( 'name' => esc_html__('Desable form','olins'), 'value' => 'desable', ),
                ),
            ),
            array(
                'name' => esc_html__('Enable services box','olins'),
                'desc' => esc_html__('Enable or disable service box on homepage','olins'),
                'id'   => $prefix . 'service_box',
                'std'  => 'enable',
                'type'    => 'select',
                'options' => array(
                    array( 'name' => esc_html__('Enable service box','olins'), 'value' => 'enable', ),
                    array( 'name' => esc_html__('Desable service box','olins'), 'value' => 'desable', ),
                ),
            ),
            array(
                'name' => esc_html__('Enable partners box','olins'),
                'desc' => esc_html__('Enable or disable partners section on homepage','olins'),
                'id'   => $prefix . 'partners_box',
                'std'  => 'enable',
                'type'    => 'select',
                'options' => array(
                    array( 'name' => esc_html__('Enable partners','olins'), 'value' => 'enable', ),
                    array( 'name' => esc_html__('Desable partners','olins'), 'value' => 'desable', ),
                ),
            ),
            array(
                'name' => esc_html__('Enable testimonials','olins'),
                'desc' => esc_html__('Enable or disable testimonials on homepage','olins'),
                'id'   => $prefix . 'testimonials_box',
                'std'  => 'enable',
                'type'    => 'select',
                'options' => array(
                    array( 'name' => esc_html__('Enable testimonials','olins'), 'value' => 'enable', ),
                    array( 'name' => esc_html__('Desable testimonials','olins'), 'value' => 'desable', ),
                ),
            ),
            array(
                'name' => esc_html__('Enable portfolio','olins'),
                'desc' => esc_html__('Enable or disable portfolio on homepage','olins'),
                'id'   => $prefix . 'portfolio_box',
                'std'  => 'enable',
                'type'    => 'select',
                'options' => array(
                    array( 'name' => esc_html__('Enable portfolio','olins'), 'value' => 'enable', ),
                    array( 'name' => esc_html__('Desable portfolio','olins'), 'value' => 'desable', ),
                ),
            ),
            array(
                'name' => esc_html__('Order box custom title','olins'),
                'desc' => esc_html__('Type here custom title for your order box','olins'),
                'id'   => $prefix . 'order_box_title',
                'std' => 'Order gardener',
                'type'    => 'text',
            ),
            array(
                'name' => esc_html__('Garderns section title','olins'),
                'desc' => esc_html__('Type here custom title gardeners section','olins'),
                'id'   => $prefix . 'gardeners_title',
                'std' => 'Gardeners',
                'type'    => 'text',
            ),
            array(
                'name' => esc_html__('Latest projects section title','olins'),
                'desc' => esc_html__('Type here custom title for latest projects section','olins'),
                'id'   => $prefix . 'latest_projects_title',
                'std' => 'Latest projects',
                'type'    => 'text',
            ),
            array(
                'name' => esc_html__('Latest projects link title','olins'),
                'desc' => esc_html__('Type here custom title for latest projects link','olins'),
                'id'   => $prefix . 'latest_projects_link_title',
                'std' => 'All projects',
                'type'    => 'text',
            ),
            array(
                'name' => esc_html__('Our partners title','olins'),
                'desc' => esc_html__('Type here custom title for partners','olins'),
                'id'   => $prefix . 'partners_title',
                'std' => 'Our partners',
                'type'    => 'text',
            ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'about_settings_metabox',
        'title'      => esc_html__('About page settings','olins'),
        'pages'      => array( 'page',), // Post type
        'context'    => 'normal',
        'priority'   => 'high',
        'show_on' => array('key' => 'page-template', 'value' => array('template-about.php'),),
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
                'name' => esc_html__('Enable additional info box','olins'),
                'desc' => esc_html__('Enable or disable additional info box','olins'),
                'id'   => $prefix . 'additional_box',
                'std'  => 'enable',
                'type'    => 'select',
                'options' => array(
                    array( 'name' => esc_html__('Enable additional','olins'), 'value' => 'enable', ),
                    array( 'name' => esc_html__('Desable additional','olins'), 'value' => 'desable', ),
                ),
            ),
            array(
                'name' => esc_html__('Enable skills box','olins'),
                'desc' => esc_html__('Enable or disable skills box','olins'),
                'id'   => $prefix . 'skills',
                'std'  => 'enable',
                'type'    => 'select',
                'options' => array(
                    array( 'name' => esc_html__('Enable skills','olins'), 'value' => 'enable', ),
                    array( 'name' => esc_html__('Desable skills','olins'), 'value' => 'desable', ),
                ),
            ),
            array(
                'name' => esc_html__('Enable video box','olins'),
                'desc' => esc_html__('Enable or disable video box','olins'),
                'id'   => $prefix . 'video_box',
                'std'  => 'enable',
                'type'    => 'select',
                'options' => array(
                    array( 'name' => esc_html__('Enable video box','olins'), 'value' => 'enable', ),
                    array( 'name' => esc_html__('Desable video box','olins'), 'value' => 'desable', ),
                ),
            ),
            array(
                'name' => esc_html__('Enable partners box','olins'),
                'desc' => esc_html__('Enable or disable partners box','olins'),
                'id'   => $prefix . 'partners',
                'std'  => 'enable',
                'type'    => 'select',
                'options' => array(
                    array( 'name' => esc_html__('Enable partners box','olins'), 'value' => 'enable', ),
                    array( 'name' => esc_html__('Desable partners box','olins'), 'value' => 'desable', ),
                ),
            ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'about_add_settings_metabox',
        'title'      => esc_html__('Additional info settings','olins'),
        'pages'      => array( 'page',), // Post type
        'context'    => 'normal',
        'priority'   => 'high',
        'show_on' => array('key' => 'page-template', 'value' => array('template-about.php'),),
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
                'name' => esc_html__('Container title','olins'),
                'desc' => esc_html__('Type here custom title','olins'),
                'id'   => $prefix . 'info_title',
                'std' => '',
                'type'    => 'text',
            ),
            array(
                'name' => esc_html__('Photo #1','olins'),
                'desc' => esc_html__('Upload photo #1','olins'),
                'id'   => $prefix . 'info_photo_one',
                'std' => '',
                'type'    => 'file',
            ),
            array(
                'name' => esc_html__('Photo #2','olins'),
                'desc' => esc_html__('Upload photo #2','olins'),
                'id'   => $prefix . 'info_photo_two',
                'std' => '',
                'type'    => 'file',
            ),
            array(
                'name' => esc_html__('Info description','olins'),
                'desc' => esc_html__('Type here description','olins'),
                'id'   => $prefix . 'info_description',
                'std' => '',
                'type'    => 'wysiwyg',
            ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'about_skills_settings_metabox',
        'title'      => esc_html__('Skills settings','olins'),
        'pages'      => array( 'page',), // Post type
        'context'    => 'normal',
        'priority'   => 'high',
        'show_on' => array('key' => 'page-template', 'value' => array('template-about.php'),),
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
                'name' => esc_html__('Icon #1','olins'),
                'desc' => esc_html__('Upload icon #1','olins'),
                'id'   => $prefix . 'skills_icon_1',
                'std' => '',
                'type'    => 'file',
            ),
            array(
                'name' => esc_html__('Title #1','olins'),
                'desc' => esc_html__('Type here custom title','olins'),
                'id'   => $prefix . 'skills_title_1',
                'std' => '',
                'type'    => 'text',
            ),
            array(
                'name' => esc_html__('Description #1','olins'),
                'desc' => esc_html__('Type here description','olins'),
                'id'   => $prefix . 'skills_description_1',
                'std' => '',
                'type'    => 'textarea',
            ),
            array(
                'name' => esc_html__('Icon #2','olins'),
                'desc' => esc_html__('Upload icon #2','olins'),
                'id'   => $prefix . 'skills_icon_2',
                'std' => '',
                'type'    => 'file',
            ),
            array(
                'name' => esc_html__('Title #2','olins'),
                'desc' => esc_html__('Type here custom title','olins'),
                'id'   => $prefix . 'skills_title_2',
                'std' => '',
                'type'    => 'text',
            ),
            array(
                'name' => esc_html__('Description #2','olins'),
                'desc' => esc_html__('Type here description','olins'),
                'id'   => $prefix . 'skills_description_2',
                'std' => '',
                'type'    => 'textarea',
            ),
            array(
                'name' => esc_html__('Icon #3','olins'),
                'desc' => esc_html__('Upload icon #3','olins'),
                'id'   => $prefix . 'skills_icon_3',
                'std' => '',
                'type'    => 'file',
            ),
            array(
                'name' => esc_html__('Title #3','olins'),
                'desc' => esc_html__('Type here custom title','olins'),
                'id'   => $prefix . 'skills_title_3',
                'std' => '',
                'type'    => 'text',
            ),
            array(
                'name' => esc_html__('Description #3','olins'),
                'desc' => esc_html__('Type here description','olins'),
                'id'   => $prefix . 'skills_description_3',
                'std' => '',
                'type'    => 'textarea',
            ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'about_video_settings_metabox',
        'title'      => esc_html__('Video box settings','olins'),
        'pages'      => array( 'page',), // Post type
        'context'    => 'normal',
        'priority'   => 'high',
        'show_on' => array('key' => 'page-template', 'value' => array('template-about.php'),),
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
                'name' => esc_html__('Video photo','olins'),
                'desc' => esc_html__('Upload video photo','olins'),
                'id'   => $prefix . 'video_photo',
                'std' => '',
                'type'    => 'file',
            ),
            array(
                'name' => esc_html__('Video link','olins'),
                'desc' => esc_html__('Type here link for your video','olins'),
                'id'   => $prefix . 'video_link',
                'std' => '',
                'type'    => 'text',
            ),
            array(
                'name' => esc_html__('Title','olins'),
                'desc' => esc_html__('Type here custom title','olins'),
                'id'   => $prefix . 'video_title',
                'std' => '',
                'type'    => 'text',
            ),
            array(
                'name' => esc_html__('Description','olins'),
                'desc' => esc_html__('Type here description','olins'),
                'id'   => $prefix . 'video_description',
                'std' => '',
                'type'    => 'textarea',
            ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'contacts_settings_metabox',
        'title'      => esc_html__('Contacts settings','olins'),
        'pages'      => array( 'page',), // Post type
        'context'    => 'normal',
        'priority'   => 'high',
        'show_on' => array('key' => 'page-template', 'value' => array('template-contacts.php'),),
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
                'name' => esc_html__('Phone label','olins'),
                'desc' => esc_html__('Insert here the phone label','olins'),
                'id'   => $prefix . 'phone_label',
                'std' => 'Phone',
                'type'    => 'text',
            ),
            array(
                'name' => esc_html__('Phone number','olins'),
                'desc' => esc_html__('Insert here the phone number','olins'),
                'id'   => $prefix . 'phone_number',
                'std' => '',
                'type'    => 'text',
            ),
            array(
                'name' => esc_html__('Email label','olins'),
                'desc' => esc_html__('Insert here the email label','olins'),
                'id'   => $prefix . 'email_label',
                'std' => 'Email',
                'type'    => 'text',
            ),
            array(
                'name' => esc_html__('Your Email','olins'),
                'desc' => esc_html__('Insert here your email','olins'),
                'id'   => $prefix . 'your_email',
                'std' => '',
                'type'    => 'text',
            ),
            array(
                'name' => esc_html__('Address label','olins'),
                'desc' => esc_html__('Insert here the adress label','olins'),
                'id'   => $prefix . 'address_label',
                'std' => 'Address',
                'type'    => 'text',
            ),
            array(
                'name' => esc_html__('Your address','olins'),
                'desc' => esc_html__('Insert here your address','olins'),
                'id'   => $prefix . 'your_address',
                'std' => '',
                'type'    => 'text',
            ),
            array(
                'name' => esc_html__('Contacts form title','olins'),
                'desc' => esc_html__('Type here form title','olins'),
                'id'   => $prefix . 'form_title',
                'std' => 'Contacts form',
                'type'    => 'text',
            ),
            array(
                'name' => esc_html__('Contacts form subtitle','olins'),
                'desc' => esc_html__('Type here form subtitle','olins'),
                'id'   => $prefix . 'form_subtitle',
                'std' => '',
                'type'    => 'text',
            ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'post_pages_metabox',
        'title'      => esc_html__('Post settings','olins'),
        'pages'      => array( 'post',), // Post type
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
                'name' => esc_html__('Sub title','gardener'),
                'desc' => esc_html__('Type here sub title for the post','gardener'),
                'id'   => $prefix . 'post_sub_title',
                'std' => '',
                'type'    => 'text',
            ),
        )
    );

	return $meta_boxes;
}

/**
 * Get image sizes for images
 * 
 * @return array
 */
function ale_get_images_sizes() {
	return array(

        'post' => array(
            array(
                'name'      => 'post-single-image',
                'width'     => 1380,
                'height'    => 550,
                'crop'      => true,
            ),
        ),
    );
}

/**
 * Add post formats that are used in theme
 * 
 * @return array
 */
function ale_get_post_formats() {
	return array('gallery','link','quote','video','audio');
}

/**
 * Get sidebars list
 * 
 * @return array
 */
function ale_get_sidebars() {
	$sidebars = array();
	return $sidebars;
}

/**
 * Post types where metaboxes should show
 * 
 * @return array
 */
function ale_get_post_types_with_gallery() {
	return array('gallery');
}

/**
 * Add custom fields for media attachments
 * @return array
 */
function ale_media_custom_fields() {
	return array();
}